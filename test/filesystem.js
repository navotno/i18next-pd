/* eslint-env node, mocha */

const assert = require('assert');
const FileSystem = require('../src/FileSystem');

describe('FileSystem', () => {
    const fileSystem = new FileSystem({
        rootDir: '.',
        ignore: ['.git', '.idea', 'node_modules']
    });

    it('Test "getComponentsDirs"', () => {
        const result = ['./lib', './src', './test'];

        assert.deepEqual(fileSystem.getComponentsDirs(), result);
    });

    it('Test "getFilesPath"', () => {
        const result = [
            './src/FileSystem.js',
            './src/Formatter.js',
            './src/Logger.js',
            './src/OneSky.js',
            './src/Scanner.js'
        ];

        assert.deepEqual(fileSystem.getFilesPath('./src'), result);
    });
});
