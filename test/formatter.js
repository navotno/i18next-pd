/* eslint-env node, mocha */

const assert = require('assert');
const _ = require('lodash');
const Formatter = require('../src/Formatter');

const formatter = new Formatter();

describe('Formatter', () => {
    it('Test "clearKey"', () => {
        const clearKeyIn = {
            test_plural_3: '',
            test_plural_11: '',
            test_plural_100: ''
        };
        const clearKeyOut = {
            test_0: '',
            test_1: '',
            test_2: ''
        };
        const result = {};

        _.mapKeys(clearKeyIn, (val, key) => {
            result[formatter.clearKey(key)] = '';
        });

        assert.deepEqual(result, clearKeyOut);
    });

    it('Test "clearVal"', () => {
        const clearValIn = {
            test_0: '%{test_0}',
            test_1: '{test_1}',
            test_2: '{{test_2}}'
        };
        const clearValOut = {
            test_0: '{{test_0}}',
            test_1: '{{test_1}}',
            test_2: '{{test_2}}'
        };
        const result = {};

        _.mapKeys(clearValIn, (val, key) => {
            result[key] = formatter.clearValue(val);
        });

        assert.deepEqual(result, clearValOut);
    });

    it('Test "oneskyOutToI18n"', () => {
        const onesky = {
            en: {
                translation: {
                    common: {
                        Машина: 'Машина',
                        Машина_plural: '%{count} Машина'
                    }
                }
            },
            ru: {
                translation: {
                    common: {
                        Машина: 'Машина',
                        Машина_plural_3: '%{count} машины',
                        Машина_plural_11: '%{count} машин',
                        Машина_plural_100: ''
                    }
                }
            }
        };
        const i18n = {
            en: {
                common: {
                    Машина: 'Машина',
                    Машина_plural: '{{count}} Машина'
                }
            },
            ru: {
                common: {
                    Машина: 'Машина',
                    Машина_0: '{{count}} машины',
                    Машина_1: '{{count}} машин',
                    Машина_2: ''
                }
            }
        };

        assert.deepEqual(formatter.oneskyOutToI18n(onesky), i18n);
    });
});
