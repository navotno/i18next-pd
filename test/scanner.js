const assert = require('assert');
const Scanner = require('../src/Scanner');

const scanner = new Scanner(
    't("common:Test string", { count: 1 })',
    {
        lngs: ['ru', 'en', 'tr', 'kk']
    }
);

describe('Scanner', () => {
    const i18nJson = {
        test: '',
        test_plural: ''
    };
    const result = {
        test: {
            one: 'test',
            other: '%{count} test'
        }
    };

    it('Test "plural"', () => {
        assert.deepEqual(scanner.plural(i18nJson), result);
    });

    it('Test "getNamespace"', () => {
        const code = 'export default translate(\'download\')(DownloadPage);';
        assert.equal(Scanner.getNamespace(code), 'download');
    });
});
