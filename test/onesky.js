/* eslint-env node, mocha */

const assert = require('assert');
const OneSky = require('../src/OneSky');

var options = {
    secret: '1234567',
    apiKey: 'abcdefg',
    projectId: '123'
};

const onesky = new OneSky(options);

describe('OneSky', () => {
    const i18json = {
        en: {
            common: {
                test: {
                    one: 'test',
                    other: '%{count} tests'
                }
            }
        }
    };

    it('Test "put"', async () => {
        let result = '';

        // await onesky.put('test.json', i18json).then((res) => {
        //     result = res;
        // });

        // assert.deepEqual(result, { message: 'Fail to authorize', code: 401 });
    });

    it('Test "get"', async () => {
        let result = '';

        await onesky.get('test.json').then((res) => {
            result = res;
        });

        assert.deepEqual(result, { message: 'Fail to authorize', code: 401 });
    });
});
