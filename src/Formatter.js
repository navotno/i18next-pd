import {mapKeys} from 'lodash';

export default class Formatter {
    /**
     * Delete postfix _plural_** from key
     * @param {String} key
     * @return {String} key
     */
    clearKey (key) {
        let result = key;
        result = result.replace('_plural_3', '_0');
        result = result.replace('_plural_11', '_1');
        result = result.replace('_plural_100', '_2');

        return result;
    }

    /**
     * Replace '%{', '{', '}' with '{{' and '}}'
     * @param val
     * @return {*}
     */
    clearValue (val) {
        return val
            .replace(/{{/gim, '\u2962')
            .replace(/}}/gim, '\u2964')
            .replace(/%{/gim, '\u2962')
            .replace(/{/gim, '\u2962')
            .replace(/}/gim, '\u2964')
            .replace(/\u2962/gim, '{{')
            .replace(/\u2964/gim, '}}');
    }

    /**
     * Restructs OneSky Json to i18n Json
     * @param {Object} onesky
     * @return {Object} i18nJson
     */
    oneskyOutToI18n (onesky) {
        const i18n = {};

        mapKeys(onesky, (NSs, lng) => {
            mapKeys(NSs.translation, (phrases, ns) => {
                const resultPhrases = {};

                if (!i18n[lng]) i18n[lng] = {};

                mapKeys(phrases, (val, key) => {
                    resultPhrases[this.clearKey(key)] = this.clearValue(val);
                });

                i18n[lng][ns] = resultPhrases;
            });
        });

        return i18n;
    }
}
