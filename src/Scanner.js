import {mapKeys} from 'lodash';
import i18next from 'i18next';
import i18nScanner from 'i18next-scanner';

const Parser = i18nScanner.Parser

export default class Scanner {
    /**
     * Get namespaces from code
     * @param {String} code
     * @param {Object} options
     * @return {String} namespace
     */
    static getNamespace (code, options = {}) {
        const defaultOptions = {
            decorator: 'translate',
            ...options
        };
        let namespace = '';
        let decorator = code.match(new RegExp(`${defaultOptions.decorator}\\('(.+)'`));

        if (decorator && decorator[1]) {
            namespace = decorator[1];
        }

        return namespace;
    }

    /**
     * @param {String} code
     * @param {Object} options
     */
    constructor (code, options = {}) {
        const defaultOptions = {
            debug: false,
            func: { list: ['t'], extensions: ['.js', '.jsx'] },
            ns: ['common'],
            lngs: [options.baseLang || 'en'],
            keySeparator: '.',
            contextSeparator: '_',
            plural: true,
            pluralSeparator: '_',
            defaultNs: 'common',
            resource: {
                loadPath: `${options.resourcePath}/i18n/{{lng}}/{{ns}}.json`
            }
        };

        this.baseLang = options.baseLang || 'en';
        this.parser = new Parser({ ...defaultOptions, ...options });
        this.parser.parseTransFromString(code);
        this.parser.parseFuncFromString(code);

        i18next.init();
    }

    /**
     * Run scanner
     * @return {Object} i18nJson
     */
    run () {
        let i18nJson = this.parser.get();

        return {
            [this.baseLang]: this.pluralizeNSs(i18nJson[this.baseLang])
        };
    }

    /**
     * Pluralize namespace
     * @param NSs
     * @return {{}}
     */
    pluralizeNSs (NSs) {
        const newNSs = {};

        for (let NSName in NSs) {
            const NSData = NSs[NSName];

            newNSs[NSName] = this.plural(NSData);
        }

        return newNSs;
    }

    /**
     *
     * @param {Object} ns i18nJson.en.{{Namespace}}
     * @return {Object} ns i18nJson.en.{{Namespace}}
     */
    plural (ns) {
        const result = {};

        mapKeys(ns, (value, key) => {
            if (key !== undefined && value !== undefined && typeof value !== 'object') {
                if (key.indexOf('_plural') > -1 || value.indexOf('{{count}}') > -1) {
                    let cleanKey = key.replace('_plural', '');

                    result[cleanKey] = {
                        one: cleanKey,
                        other: `%{count} ${cleanKey}`
                    };
                } else {
                    result[key] = value || key;
                }
            }
        });

        return result;
    }
}
