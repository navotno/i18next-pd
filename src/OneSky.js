import onesky from 'onesky-utils';

export default class OneSky {
    constructor (options = {}) {
        this.options = options;
    }

    /**
     * Put file to OnySky
     * @param {String} fileName
     * @param {Object} i18nJson
     * @param {Object} options
     * @return {Promise.<{Object}>}
     */
    async put (fileName = '', i18nJson = {}, options = {}) {
        const content = typeof i18nJson === 'string' ? i18nJson : JSON.stringify(i18nJson);
        let result = {};

        await onesky.postFile({
            format: 'MULTILINGUAL_JSON',
            keepStrings: true,
            language: 'en',
            ...this.options,
            ...options,
            fileName,
            content
        }).then((content) => {
            result = content;
        }).catch((err) => {
            result = err;
        });

        return result;
    }

    /**
     * Get file from OneSky
     * @param {String} filename
     * @param {Object} options
     * @return {Promise.<{Object}>}
     */
    async get (filename, options = {}) {
        let result = {};

        await onesky.getMultilingualFile({
            format: 'I18NEXT_MULTILINGUAL_JSON',
            ...this.options,
            ...options,
            fileName: filename
        }).then(i18nData => {
            result = JSON.parse(i18nData);
        }).catch((error) => {
            result = error;
        });

        return result;
    }
}
