import fs from 'fs';
import {mapKeys} from 'lodash';
import path from 'path';
import Scanner from './Scanner';

export default class FileSystem {
    constructor (options = {}) {
        this.options = {
            src: [ '.' ],
            output: '.',
            extensions: ['js'],
            ignore: [],
            ...options
        };
    }

    /**
     * Run build script
     * @return {Object} result
     */
    build () {
        this.options.extensions = ['json'];
        let components = [];
        this.options.src.forEach((dir) => {
            components = [ ...components, ...this.getComponentsDirs(dir.path) ];
        });

        let i18nextJson = {};
        const files = [];
        components.forEach((component) => {
            if (fs.existsSync(component + '/i18n')) {
                this.getFilesPath(component + '/i18n').forEach((file) => {
                    const ns = JSON.parse(fs.readFileSync(file, 'utf8').toString());
                    const regexp = new RegExp(`${component}/i18n/([a-zA-Z]+).json`);
                    const lang = file.match(regexp)[1];

                    if (!i18nextJson[lang]) i18nextJson[lang] = {};
                    Object.keys(ns).forEach((key) => {
                        i18nextJson[lang][key] = ns[key];
                    });
                });
            }
        });

        if (!fs.existsSync(this.options.output)) {
            fs.mkdirSync(this.options.output);
        }

        Object.keys(i18nextJson).forEach((lang) => {
            fs.writeFileSync(
                this.options.output + '/' + lang + '.js',
                'export default ' + JSON.stringify(i18nextJson[lang], null, '  ')
            );
            files.push(this.options.output + '/' + lang + '.js');
        });

        return {
            i18nextJson,
            files
        };
    }

    /**
     * Run automatically directory scanning
     * @return {Object} result
     */
    run () {
        let components = [];
        this.options.src.forEach((dir) => {
            components = [ ...components, ...this.getComponentsDirs(dir.path) ];
        });

        const result = {};
        const namespaces = new Set();
        components.forEach((component) => {
            const item = {
                name: path.basename(component),
                path: component,
                files: this.getFilesPath(component),
                namespace: Scanner.getNamespace(
                    this.getFilesContent(
                        this.getFilesPath(component)
                    )
                )
            };
            if (item.namespace) {
                result[item.namespace] = item;
                namespaces.add(item.namespace);
                item.content = this.getFilesContent(item.files);
            }
        });

        return {
            components: result,
            namespaces: [...namespaces]
        };
    }

    /**
     * Get components dirs
     * @param {String} dir
     * @return {Array} files
     */
    getComponentsDirs (dir) {
        const files = fs.readdirSync(dir);
        const result = [];

        files.forEach((file) => {
            const name = dir + '/' + file;

            if (this.options.ignore.indexOf(file) < 0) {
                if (fs.statSync(name).isDirectory()) {
                    result.push(name);
                }
            }
        });

        return result;
    }

    /**
     * Scan directory and get all files path
     * @param {String} dir
     * @return {Array} files
     */
    getFilesPath (dir = this.options.src) {
        const files = fs.readdirSync(dir);
        let result = [];

        files.forEach((file) => {
            const name = dir + '/' + file;

            if (this.options.ignore.indexOf(file) < 0) {
                if (fs.statSync(name).isDirectory()) {
                    result = [...result, ...this.getFilesPath(name)];
                } else if (
                    !this.options.extensions.length ||
                    this.options.extensions.indexOf(path.extname(name).substr(1)) > -1
                ) {
                    result.push(name);
                }
            }
        });

        return result;
    }

    /**
     * Get files content
     * @param files
     * @return {string}
     */
    getFilesContent (files = []) {
        let result = '';
        files.forEach((filePath) => {
            if (fs.statSync(filePath).isFile()) {
                result += fs.readFileSync(filePath).toString();
            }
        });
        return result;
    }

    /**
     * Write i18nJson to component directory
     * @param {String} path
     * @param {Object} i18nJson
     * @return {Array} files
     */
    writeToFile (path, i18nJson) {
        const res = [];
        mapKeys(i18nJson, (ns, lang) => {
            if (!fs.existsSync(path + '/i18n')) {
                fs.mkdirSync(path + '/i18n');
            }

            fs.writeFileSync(path + '/i18n/' + lang + '.json', JSON.stringify(ns, null, '  '));
            res.push('i18n/' + lang + '.json');
        });
        return res;
    }

    /**
     * Write osJson to component directory or root directory
     * @param {Object} osJson
     * @return {Array} files
     */
    writeOSJson (osJson) {
        const res = [];

        mapKeys(osJson, (NSs, lang) => {
            mapKeys(NSs, (content, ns) => {
                let path = '';

                this.options.src.forEach(dir => {
                    let component = '';

                    if (dir.prefix) { // && dir.prefix === ns.substr(0, dir.prefix.length)) {
                        component = ns.substr(dir.prefix.length);
                        component = component[0].toUpperCase() + component.substr(1);
                    } else {
                        component = ns[0].toUpperCase() + ns.substr(1);
                    }

                    if (fs.existsSync(dir.path + '/' + component)) {
                        path = dir.path + '/' + component;
                    }
                });

                if (path) {
                    if (!fs.existsSync(`${path}/i18n`)) {
                        fs.mkdirSync(`${path}/i18n`);
                    }

                    fs.writeFileSync(`${path}/i18n/${lang}.json`, JSON.stringify({
                        [ns]: content
                    }, null, '  '));
                    res.push(`${path}/i18n/${lang}.json`);
                }
            });
        });
        return res;
    }

    writeToDist (osJson) {
        const res = [];
        mapKeys(osJson, (NSs, lang) => {
            let path = this.options.output;

            if (!fs.existsSync(path)) fs.mkdirSync(path);

            fs.writeFileSync(`${path}/${lang}.js`, 'export default ' + JSON.stringify(NSs, null, '  '));
            res.push(`${path}/${lang}.js`);
        });
        return res;
    }
}
