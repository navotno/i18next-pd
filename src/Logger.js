const chalk = require('chalk');

const log = console.log;

module.exports = {
    clean: () => log('\u001b[2J\u001b[1;1H'),
    bold: (text, hex = '#fff') => log(chalk.bold.hex(hex)(text)),
    boldKV: (key, val, keyHex = '#00a73d', valHex = '#aa9d00') => log(
        chalk.bold.hex(keyHex)(key),
        chalk.reset.hex(valHex)(val)
    ),
    reset: (text, hex = '#fff') => log(chalk.reset.hex(hex)(text)),
    boldDim: (text, hex = '#fff') => log(chalk.bold.dim.hex(hex)(text)),
    osg: (filename, hex = '#fff') => log(
        chalk.bold.keyword('white')('\nGet file '),
        chalk.underline.keyword('white')(filename),
        chalk.bold.keyword('white')(' from OneSky')
    ),
    osp: (filename, hex = '#fff') => log(
        chalk.bold.keyword('white')('\nPut file '),
        chalk.underline.keyword('white')(filename),
        chalk.bold.keyword('white')(' to OneSky')
    ),
    upt: (time) => log(
        chalk.bold.hex('#00a73d')('\nUptime: '),
        chalk.reset.hex('#d55413')(`${time} ms`)
    ),
    error: (err, time) => {
        log(chalk.bold.hex('#d55413')('     ✖ Unsuccessful'));
        log(err);
        log(
            chalk.bold.hex('#00a73d')('\nUptime: '),
            chalk.reset.hex('#d55413')(`${time} ms`)
        );
    }
};
