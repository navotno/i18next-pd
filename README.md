# i18next-pd
Данная библиотека разработана с целью облегчения и ускорения работы с фреймворком интернацианализации [i18next](http://www.i18next.com/).

#### Возможности i18next-pd:
- Сканировать код н апредмет содержания функций интернационализации
- Соберать результаты сканирования в JSON объекты для дальнейшей обработки
- Реструктурировать JSON корректной комуникации между сервисами
- Выгружать и обратно загружать JSON в обланый сервис [OneSky](http://www.oneskyapp.com/)

#### Структура библиотеки:
```
    /lib - Дериктория с бандлами
    
    /src - Дериктория с исходными файлами
        FileSystem - Модуль для работы с файловой системой
        Formatter - Модуль для реструктуризации JSON
        Logger - Модуль для логгирования
        OneSky - Модуль для рбота с OneSky API
        Scanner - Модуль сканирования кода
    
    /test - Дериктория с тестами
        filesystem - Тесты модуля FileSystem
        formatter - Тесты модуля Formatter
        onesky - Тесты модуля OneSky
        scanner - Тесты модуля Scanner
    
    config.json - Файл конфигурации
    index.js - Точка входа
```

## Scanner API
Данный модуль предназначен для сканирования входящего кода (в текстовом формате) на предмет соержания функций интернацианализации **i18n**и генерации JSON объекта переводов.

Для корректного определения **namespace,** в экспортирование компонента должно быть обернуто в метод **translate,** как в нижеуказанном примере:

```
export default translate('NAMESPACE')(COMPONENT);
```
Где:
* **NAMESPACE** - соответствует названию компонента (указывается с маленькой буквы)
* **COMPONENT** - название компонента

### Пример

Параметры:
* **CODE** - String
* **Options** - Object

Ответ:
* **i18nJson** - Object

```js
const Scanner = require('./src/Scanner');

const options = { baseLang: ['en'] };
const scanner = new Scanner(CODE, options);

const i18json = scanner.run();
```

Если **baseLang** не определен, то стандартным языком будет установлен **Английский.** Стандартной функцией интернацианализации является **t(),** но Вы можете определить любую другую функцию, указав ее в опциях.

### Пример результата работы сканера

#### Входящий код
```html
<div>
    <h1>t("Ball", { count: 1 })</h1>
    <span>
        t("Car", { count: 1 })
    </span>
    t("Home")
</div>
...
export default translate('home')(Home);
```

#### Результирующий JSON объект
```json
{
    "en": {
        "home": {
            "Ball": {
                "one": "Ball",
                "other": "%{count} Ball"
            },
            "Car": {
                "one": "Car",
                "other": "%{count} Car"
            },
            "Home": ""
        }
    }
}
```

## Formatter API
Данный модуль предназначен для реструктуризации JSON объектов, с целью облегчения взаимодействия между модулями и сервисами.

### Стандартный API

```js
const Formatter = require('./src/Formatter');
const formatter = new Formatter();
```

#### formatter.oneskyOutToI18n
Метод предназначен для реструктуризации мультиязычного JSON объекта приходящего из облачного сервиса**OneSky,** в объект интернацианализации**i18next**

Параметры:
* **OneSkyJson** - Object

Ответ:
* **i18nJson** - Object

```js
const result = formatter.oneskyOutToI18n(OneSkyJson);
```

##### OneSky JSON объект
```json
{
    "en": {
        "translation": {
            "common": {
                "Ball": "Ball",
                "Ball_plural": "%{count} balls"
            }
        }
    },
    "ru": {
        "translation": {
            "common": {
                "Ball": "Мяч",
                "Ball_plural_3": "%{count} мяч",
                "Ball_plural_11": "%{count} мяча",
                "Ball_plural_100": "%{count} мячей"
            }
        }
    }
}
```

##### Результирующий i18n JSON объект

```json
{
    "en": {
        "common": {
            "Ball": "Ball",
            "Ball_plural": "{{count}} balls"
        }
    },
    "ru": {
        "common": {
            "Ball": "Мяч",
            "Ball_0": "{{count}} мяч",
            "Ball_1": "{{count}} мяча",
            "Ball_2": "{{count}} мячей"
        }
    }
}
```

#### formatter.clearKey
Метод предназначеный для очистки ключа от постфиксов (\_plural_**), генерируемых сервисом OneSky для перевовдов с плюрализацией.

Параметры:
* **Key** - String

Ответ:
* **CleanKey** - String

```js
const cleanKey = formatter.clearKey(key);
```

##### Входящие ключи
```js
Ball_plural_3
Ball_plural_11
Ball_plural_100
```

##### Обработанные ключи
```js
Ball_0
Ball_1
Ball_2
```

#### formatter.clearValue
Метод предназначеный для замены элементов шаблона '%{' и '}', генерируемые сервисом OneSky, на '{{' и '}}' соответственно.

Параметры:
* **Value** - String

Ответ:
* **CleanValue** - String

```js
const cleanValue = formatter.clearValue(value);
```

##### Входящее значение
```
%{count} balls
```

##### Обработанное значение
```
{{count}} balls
```

## FileSystem API
Данный модуль предназначен для работы с файловой системой, сканирования и записи файлов переводов.

### Стандартный API

```js
const fileSystem = new FileSystem({
    "src": "./src/components",
    "output": "./public/i18n",
    "ignore": [
      "node_modules",
      "index.js"
    ]
});
const { components } = fileSystem.run();
```
Где:
* **src** - путь к дериктории с компонентами
* **output** - путь к дериктории с итоговыми файлами переводов
* **ignore** - список файлов и дерикторий которые игнорируются системой при сканировании

#### fileSystem.run
Метод предназначен для сканирования директории компонентов, генерации объекта с информацией о компоненте, списком используемых namespace и кодом компонента в текстовом формате.

Ответ:
* **Result** - Object

```js
const result = fileSystem.run();
```

##### Пример результата работы метода run

```json
{
    "components": {
        "download": {
            "name": "Home",
            "path": "./src/components/Home",
            "files": [
                "./src/components/Home/index.js"
            ],
            "namespace": "home",
            "content": "CODE"
        }
    },
    "namespaces": [
      "home"
    ]
}
```

#### fileSystem.getComponentsDirs
Метод предназначен для сканирования родительской директории компонентов и генерации массива путей к компонентам.

Параметры:
* **DIR** - String (Optional)

Ответ:
* **Dirs** - Array

```js
const dirs = fileSystem.getComponentsDirs();
```

##### Результирующий массив путей
```js
[
    './src/components/Home',
    './src/components/Download'
]
```

#### fileSystem.getFilesPath
Метод предназначен для сканирования директории компонента и генерации массива путей к вложеным файлам.

Параметры:
* **DIR** - String (Optional)

Ответ:
* **Files** - Array

```js
const files = fileSystem.getFilesPath();
```

##### Результирующий массив путей
```js
[
    './src/components/Home/index.js'
]
```

#### fileSystem.getFilesContent
Метод предназначен для получения содержимого файлов.

Параметры:
* **Files** - Array {String}

Ответ:
* **Content** - String

```js
const content = fileSystem.getFilesContent();
```

##### Входящий массив путей
```js
[
    './src/components/Home/index.js'
]
```

##### Результатом будет строка с содержимым файлов

