/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = __webpack_require__(0);

var _i18next = __webpack_require__(10);

var _i18next2 = _interopRequireDefault(_i18next);

var _i18nextScanner = __webpack_require__(11);

var _i18nextScanner2 = _interopRequireDefault(_i18nextScanner);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Parser = _i18nextScanner2.default.Parser;

var Scanner = function () {
    _createClass(Scanner, null, [{
        key: 'getNamespace',

        /**
         * Get namespaces from code
         * @param {String} code
         * @param {Object} options
         * @return {String} namespace
         */
        value: function getNamespace(code) {
            var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

            var defaultOptions = _extends({
                decorator: 'translate'
            }, options);
            var namespace = '';
            var decorator = code.match(new RegExp(defaultOptions.decorator + '\\(\'(.+)\''));

            if (decorator && decorator[1]) {
                namespace = decorator[1];
            }

            return namespace;
        }

        /**
         * @param {String} code
         * @param {Object} options
         */

    }]);

    function Scanner(code) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        _classCallCheck(this, Scanner);

        var defaultOptions = {
            debug: false,
            func: { list: ['t'], extensions: ['.js', '.jsx'] },
            ns: ['common'],
            lngs: [options.baseLang || 'en'],
            keySeparator: '.',
            contextSeparator: '_',
            plural: true,
            pluralSeparator: '_',
            defaultNs: 'common',
            resource: {
                loadPath: options.resourcePath + '/i18n/{{lng}}/{{ns}}.json'
            }
        };

        this.baseLang = options.baseLang || 'en';
        this.parser = new Parser(_extends({}, defaultOptions, options));
        this.parser.parseTransFromString(code);
        this.parser.parseFuncFromString(code);

        _i18next2.default.init();
    }

    /**
     * Run scanner
     * @return {Object} i18nJson
     */


    _createClass(Scanner, [{
        key: 'run',
        value: function run() {
            var i18nJson = this.parser.get();

            return _defineProperty({}, this.baseLang, this.pluralizeNSs(i18nJson[this.baseLang]));
        }

        /**
         * Pluralize namespace
         * @param NSs
         * @return {{}}
         */

    }, {
        key: 'pluralizeNSs',
        value: function pluralizeNSs(NSs) {
            var newNSs = {};

            for (var NSName in NSs) {
                var NSData = NSs[NSName];

                newNSs[NSName] = this.plural(NSData);
            }

            return newNSs;
        }

        /**
         *
         * @param {Object} ns i18nJson.en.{{Namespace}}
         * @return {Object} ns i18nJson.en.{{Namespace}}
         */

    }, {
        key: 'plural',
        value: function plural(ns) {
            var result = {};

            (0, _lodash.mapKeys)(ns, function (value, key) {
                if (key !== undefined && value !== undefined && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) !== 'object') {
                    if (key.indexOf('_plural') > -1 || value.indexOf('{{count}}') > -1) {
                        var cleanKey = key.replace('_plural', '');

                        result[cleanKey] = {
                            one: cleanKey,
                            other: '%{count} ' + cleanKey
                        };
                    } else {
                        result[key] = value || key;
                    }
                }
            });

            return result;
        }
    }]);

    return Scanner;
}();

exports.default = Scanner;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(5);
module.exports = __webpack_require__(6);


/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("babel-polyfill");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _lodash = __webpack_require__(0);

var _path = __webpack_require__(1);

var _path2 = _interopRequireDefault(_path);

var _fs = __webpack_require__(2);

var _fs2 = _interopRequireDefault(_fs);

var _commandLineArgs = __webpack_require__(7);

var _commandLineArgs2 = _interopRequireDefault(_commandLineArgs);

var _Logger = __webpack_require__(8);

var _Logger2 = _interopRequireDefault(_Logger);

var _Scanner = __webpack_require__(3);

var _Scanner2 = _interopRequireDefault(_Scanner);

var _OneSky = __webpack_require__(12);

var _OneSky2 = _interopRequireDefault(_OneSky);

var _Formatter = __webpack_require__(14);

var _Formatter2 = _interopRequireDefault(_Formatter);

var _FileSystem = __webpack_require__(15);

var _FileSystem2 = _interopRequireDefault(_FileSystem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var optionDefinitions = [{ name: 'scan', alias: 's', type: Boolean }, { name: 'build', alias: 'b', type: Boolean }, { name: 'component', alias: 'c', type: String }, { name: 'osp', type: String }, { name: 'osg', type: String }, { name: 'config', type: String }, { name: 'debug', alias: 'd', type: Boolean }, { name: 'write', alias: 'w', type: Boolean }];

var args = (0, _commandLineArgs2.default)(optionDefinitions);
var config = {};

try {
    var configPath = _path2.default.join(process.cwd(), '/', args.config);
    config = JSON.parse(_fs2.default.readFileSync(configPath));
    _Logger2.default.bold('✓ Config loaded', '#00a73d');
    _Logger2.default.bold('Path: ' + configPath, '#00a73d');
} catch (e) {
    _Logger2.default.bold('✖ Config file not found\n', '#d55413');
    _Logger2.default.reset('Path: ' + _path2.default.join(process.cwd(), '/', args.config || ''), '#d55413');
    _Logger2.default.reset('' + e, '#d55413');
    process.exit();
}

var fileSystem = new _FileSystem2.default(config.FileSystem);
var onesky = new _OneSky2.default(config.OneSky);
var formatter = new _Formatter2.default();

var i18nJson = { en: {} };

// Log.clean();

var setConfig = function setConfig(options) {
    config = options;
};
var getUptime = function getUptime() {
    return (process.uptime() * 1000).toString().substr(0, 3);
};
var oneSkyGet = function oneSkyGet(filename) {
    var start = process.uptime();

    _Logger2.default.osg(filename);

    onesky.get(filename).then(function (res) {
        var time = ((process.uptime() - start) * 1000).toString().substr(0, 3);
        var data = formatter.oneskyOutToI18n(res);

        _Logger2.default.bold('     ✓ Successful', '#00a73d');
        _Logger2.default.bold('       (' + time + 'ms)', '#d55413');

        if (args.debug) {
            _Logger2.default.boldDim('\nResult:');
            _Logger2.default.reset(JSON.stringify(data, null, 4), '#aa9d00');
        }

        if (args.write) {
            var srcFiles = fileSystem.writeOSJson(data);

            _Logger2.default.boldDim('\nRecorded source files:');
            srcFiles.forEach(function (file) {
                return _Logger2.default.reset('         ' + file, '#aa9d00');
            });
        }

        _Logger2.default.upt(getUptime());
    }).catch(function (err) {
        return _Logger2.default.error(err, getUptime());
    });
};
var scan = function scan() {
    _Logger2.default.bold('Scanning project');
    if (!args.debug) _Logger2.default.boldDim('Components:');

    var _fileSystem$run = fileSystem.run(),
        components = _fileSystem$run.components;

    (0, _lodash.mapKeys)(components, function (component, ns) {
        if (args.component && args.component !== component.name) return;

        var scanner = new _Scanner2.default(component.content, {
            ns: [ns],
            defaultNs: ns,
            func: { list: ['t'], extensions: ['.js', '.jsx'] },
            resourcePath: component.path
        });
        var scanResult = scanner.run();

        (0, _lodash.assign)(i18nJson.en, scanResult.en);

        var files = [];
        if (args.write) {
            files = fileSystem.writeToFile(component.path, scanResult);
        }

        if (args.debug) {
            _Logger2.default.bold('\nComponent name:', '#00a73d');
            _Logger2.default.reset('     ' + component.name, '#aa9d00');
            _Logger2.default.bold('\nNamespace:', '#00a73d');
            _Logger2.default.reset('     ' + ns, '#aa9d00');

            _Logger2.default.bold('\nComponent path:', '#00a73d');
            _Logger2.default.reset('     ' + component.path, '#aa9d00');

            if (files.length) {
                _Logger2.default.bold('\nRecorded files:', '#00a73d');
                _Logger2.default.reset(files.join(', '), '#aa9d00');
            }

            _Logger2.default.bold('\nScanning result:', '#00a73d');
            _Logger2.default.reset(JSON.stringify(scanResult, null, 4), '#aa9d00');
        } else {
            _Logger2.default.bold('     \u2713 ' + component.name, '#00a73d');
        }
    });
};
var oneSkyPut = function oneSkyPut(filename) {
    var start = process.uptime();
    scan();

    _Logger2.default.osp(filename);

    onesky.put(filename, i18nJson).then(function (res) {
        var time = ((process.uptime() - start) * 1000).toString().substr(0, 3);
        var result = JSON.parse(res.toString());

        if (result.meta.status !== 201) {
            _Logger2.default.bold('\n     \u2716 Unsuccessful (' + time + 'ms)', '#d55413');
        } else {
            _Logger2.default.bold('     ✓ Successful', '#00a73d');
            _Logger2.default.bold('       (' + time + 'ms)', '#d55413');

            if (args.debug) {
                var data = result.data;
                _Logger2.default.boldKV('     ID: ', data.import.id);
                _Logger2.default.boldKV('     Name: ', data.name);
                _Logger2.default.boldKV('     Format: ', data.format);
                _Logger2.default.boldKV('     Language: ', data.language.local_name);
            }

            _Logger2.default.upt(getUptime());
        }
    }).catch(function (err) {
        return _Logger2.default.error(err, getUptime());
    });
};
var build = function build() {
    var result = fileSystem.build();

    _Logger2.default.bold('\nBuilding i18next bundles');
    _Logger2.default.bold('\nRecorded files:', '#00a73d');
    _Logger2.default.reset('    ' + result.files.join('\n    '), '#aa9d00');

    if (args.debug) {
        _Logger2.default.bold('\nResult i18nextJSON:', '#00a73d');
        _Logger2.default.reset(JSON.stringify(result.i18nextJson, null, '  '), '#aa9d00');
    }
};

if (args.build) build();
if (args.scan) scan();
if (args.osg) oneSkyGet(args.osg);
if (args.osp) oneSkyPut(args.osp);

if (!args.osp && !args.osg) _Logger2.default.upt(getUptime());

module.exports = {
    scan: scan,
    oneSkyGet: oneSkyGet,
    oneSkyPut: oneSkyPut,
    setConfig: setConfig
};

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("command-line-args");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var chalk = __webpack_require__(9);

var log = console.log;

module.exports = {
    clean: function clean() {
        return log('\x1B[2J\x1B[1;1H');
    },
    bold: function bold(text) {
        var hex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '#fff';
        return log(chalk.bold.hex(hex)(text));
    },
    boldKV: function boldKV(key, val) {
        var keyHex = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '#00a73d';
        var valHex = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '#aa9d00';
        return log(chalk.bold.hex(keyHex)(key), chalk.reset.hex(valHex)(val));
    },
    reset: function reset(text) {
        var hex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '#fff';
        return log(chalk.reset.hex(hex)(text));
    },
    boldDim: function boldDim(text) {
        var hex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '#fff';
        return log(chalk.bold.dim.hex(hex)(text));
    },
    osg: function osg(filename) {
        var hex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '#fff';
        return log(chalk.bold.keyword('white')('\nGet file '), chalk.underline.keyword('white')(filename), chalk.bold.keyword('white')(' from OneSky'));
    },
    osp: function osp(filename) {
        var hex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '#fff';
        return log(chalk.bold.keyword('white')('\nPut file '), chalk.underline.keyword('white')(filename), chalk.bold.keyword('white')(' to OneSky'));
    },
    upt: function upt(time) {
        return log(chalk.bold.hex('#00a73d')('\nUptime: '), chalk.reset.hex('#d55413')(time + ' ms'));
    },
    error: function error(err, time) {
        log(chalk.bold.hex('#d55413')('     ✖ Unsuccessful'));
        log(err);
        log(chalk.bold.hex('#00a73d')('\nUptime: '), chalk.reset.hex('#d55413')(time + ' ms'));
    }
};

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("chalk");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("i18next");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("i18next-scanner");

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _oneskyUtils = __webpack_require__(13);

var _oneskyUtils2 = _interopRequireDefault(_oneskyUtils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var OneSky = function () {
    function OneSky() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        _classCallCheck(this, OneSky);

        this.options = options;
    }

    /**
     * Put file to OnySky
     * @param {String} fileName
     * @param {Object} i18nJson
     * @param {Object} options
     * @return {Promise.<{Object}>}
     */


    _createClass(OneSky, [{
        key: 'put',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var fileName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
                var i18nJson = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
                var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
                var content, result;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                content = typeof i18nJson === 'string' ? i18nJson : JSON.stringify(i18nJson);
                                result = {};
                                _context.next = 4;
                                return _oneskyUtils2.default.postFile(_extends({
                                    format: 'MULTILINGUAL_JSON',
                                    keepStrings: true,
                                    language: 'en'
                                }, this.options, options, {
                                    fileName: fileName,
                                    content: content
                                })).then(function (content) {
                                    result = content;
                                }).catch(function (err) {
                                    result = err;
                                });

                            case 4:
                                return _context.abrupt('return', result);

                            case 5:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function put() {
                return _ref.apply(this, arguments);
            }

            return put;
        }()

        /**
         * Get file from OneSky
         * @param {String} filename
         * @param {Object} options
         * @return {Promise.<{Object}>}
         */

    }, {
        key: 'get',
        value: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(filename) {
                var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
                var result;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                result = {};
                                _context2.next = 3;
                                return _oneskyUtils2.default.getMultilingualFile(_extends({
                                    format: 'I18NEXT_MULTILINGUAL_JSON'
                                }, this.options, options, {
                                    fileName: filename
                                })).then(function (i18nData) {
                                    result = JSON.parse(i18nData);
                                }).catch(function (error) {
                                    result = error;
                                });

                            case 3:
                                return _context2.abrupt('return', result);

                            case 4:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function get(_x5) {
                return _ref2.apply(this, arguments);
            }

            return get;
        }()
    }]);

    return OneSky;
}();

exports.default = OneSky;

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("onesky-utils");

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = __webpack_require__(0);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Formatter = function () {
    function Formatter() {
        _classCallCheck(this, Formatter);
    }

    _createClass(Formatter, [{
        key: 'clearKey',

        /**
         * Delete postfix _plural_** from key
         * @param {String} key
         * @return {String} key
         */
        value: function clearKey(key) {
            var result = key;
            result = result.replace('_plural_3', '_0');
            result = result.replace('_plural_11', '_1');
            result = result.replace('_plural_100', '_2');

            return result;
        }

        /**
         * Replace '%{', '{', '}' with '{{' and '}}'
         * @param val
         * @return {*}
         */

    }, {
        key: 'clearValue',
        value: function clearValue(val) {
            return val.replace(/{{/gim, '\u2962').replace(/}}/gim, '\u2964').replace(/%{/gim, '\u2962').replace(/{/gim, '\u2962').replace(/}/gim, '\u2964').replace(/\u2962/gim, '{{').replace(/\u2964/gim, '}}');
        }

        /**
         * Restructs OneSky Json to i18n Json
         * @param {Object} onesky
         * @return {Object} i18nJson
         */

    }, {
        key: 'oneskyOutToI18n',
        value: function oneskyOutToI18n(onesky) {
            var _this = this;

            var i18n = {};

            (0, _lodash.mapKeys)(onesky, function (NSs, lng) {
                (0, _lodash.mapKeys)(NSs.translation, function (phrases, ns) {
                    var resultPhrases = {};

                    if (!i18n[lng]) i18n[lng] = {};

                    (0, _lodash.mapKeys)(phrases, function (val, key) {
                        resultPhrases[_this.clearKey(key)] = _this.clearValue(val);
                    });

                    i18n[lng][ns] = resultPhrases;
                });
            });

            return i18n;
        }
    }]);

    return Formatter;
}();

exports.default = Formatter;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _fs = __webpack_require__(2);

var _fs2 = _interopRequireDefault(_fs);

var _lodash = __webpack_require__(0);

var _path = __webpack_require__(1);

var _path2 = _interopRequireDefault(_path);

var _Scanner = __webpack_require__(3);

var _Scanner2 = _interopRequireDefault(_Scanner);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FileSystem = function () {
    function FileSystem() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        _classCallCheck(this, FileSystem);

        this.options = _extends({
            src: ['.'],
            output: '.',
            extensions: ['js'],
            ignore: []
        }, options);
    }

    /**
     * Run build script
     * @return {Object} result
     */


    _createClass(FileSystem, [{
        key: 'build',
        value: function build() {
            var _this = this;

            this.options.extensions = ['json'];
            var components = [];
            this.options.src.forEach(function (dir) {
                components = [].concat(_toConsumableArray(components), _toConsumableArray(_this.getComponentsDirs(dir.path)));
            });

            var i18nextJson = {};
            var files = [];
            components.forEach(function (component) {
                if (_fs2.default.existsSync(component + '/i18n')) {
                    _this.getFilesPath(component + '/i18n').forEach(function (file) {
                        var ns = JSON.parse(_fs2.default.readFileSync(file, 'utf8').toString());
                        var regexp = new RegExp(component + '/i18n/([a-zA-Z]+).json');
                        var lang = file.match(regexp)[1];

                        if (!i18nextJson[lang]) i18nextJson[lang] = {};
                        Object.keys(ns).forEach(function (key) {
                            i18nextJson[lang][key] = ns[key];
                        });
                    });
                }
            });

            if (!_fs2.default.existsSync(this.options.output)) {
                _fs2.default.mkdirSync(this.options.output);
            }

            Object.keys(i18nextJson).forEach(function (lang) {
                _fs2.default.writeFileSync(_this.options.output + '/' + lang + '.js', 'export default ' + JSON.stringify(i18nextJson[lang], null, '  '));
                files.push(_this.options.output + '/' + lang + '.js');
            });

            return {
                i18nextJson: i18nextJson,
                files: files
            };
        }

        /**
         * Run automatically directory scanning
         * @return {Object} result
         */

    }, {
        key: 'run',
        value: function run() {
            var _this2 = this;

            var components = [];
            this.options.src.forEach(function (dir) {
                components = [].concat(_toConsumableArray(components), _toConsumableArray(_this2.getComponentsDirs(dir.path)));
            });

            var result = {};
            var namespaces = new Set();
            components.forEach(function (component) {
                var item = {
                    name: _path2.default.basename(component),
                    path: component,
                    files: _this2.getFilesPath(component),
                    namespace: _Scanner2.default.getNamespace(_this2.getFilesContent(_this2.getFilesPath(component)))
                };
                if (item.namespace) {
                    result[item.namespace] = item;
                    namespaces.add(item.namespace);
                    item.content = _this2.getFilesContent(item.files);
                }
            });

            return {
                components: result,
                namespaces: [].concat(_toConsumableArray(namespaces))
            };
        }

        /**
         * Get components dirs
         * @param {String} dir
         * @return {Array} files
         */

    }, {
        key: 'getComponentsDirs',
        value: function getComponentsDirs(dir) {
            var _this3 = this;

            var files = _fs2.default.readdirSync(dir);
            var result = [];

            files.forEach(function (file) {
                var name = dir + '/' + file;

                if (_this3.options.ignore.indexOf(file) < 0) {
                    if (_fs2.default.statSync(name).isDirectory()) {
                        result.push(name);
                    }
                }
            });

            return result;
        }

        /**
         * Scan directory and get all files path
         * @param {String} dir
         * @return {Array} files
         */

    }, {
        key: 'getFilesPath',
        value: function getFilesPath() {
            var _this4 = this;

            var dir = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.options.src;

            var files = _fs2.default.readdirSync(dir);
            var result = [];

            files.forEach(function (file) {
                var name = dir + '/' + file;

                if (_this4.options.ignore.indexOf(file) < 0) {
                    if (_fs2.default.statSync(name).isDirectory()) {
                        result = [].concat(_toConsumableArray(result), _toConsumableArray(_this4.getFilesPath(name)));
                    } else if (!_this4.options.extensions.length || _this4.options.extensions.indexOf(_path2.default.extname(name).substr(1)) > -1) {
                        result.push(name);
                    }
                }
            });

            return result;
        }

        /**
         * Get files content
         * @param files
         * @return {string}
         */

    }, {
        key: 'getFilesContent',
        value: function getFilesContent() {
            var files = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

            var result = '';
            files.forEach(function (filePath) {
                if (_fs2.default.statSync(filePath).isFile()) {
                    result += _fs2.default.readFileSync(filePath).toString();
                }
            });
            return result;
        }

        /**
         * Write i18nJson to component directory
         * @param {String} path
         * @param {Object} i18nJson
         * @return {Array} files
         */

    }, {
        key: 'writeToFile',
        value: function writeToFile(path, i18nJson) {
            var res = [];
            (0, _lodash.mapKeys)(i18nJson, function (ns, lang) {
                if (!_fs2.default.existsSync(path + '/i18n')) {
                    _fs2.default.mkdirSync(path + '/i18n');
                }

                _fs2.default.writeFileSync(path + '/i18n/' + lang + '.json', JSON.stringify(ns, null, '  '));
                res.push('i18n/' + lang + '.json');
            });
            return res;
        }

        /**
         * Write osJson to component directory or root directory
         * @param {Object} osJson
         * @return {Array} files
         */

    }, {
        key: 'writeOSJson',
        value: function writeOSJson(osJson) {
            var _this5 = this;

            var res = [];

            (0, _lodash.mapKeys)(osJson, function (NSs, lang) {
                (0, _lodash.mapKeys)(NSs, function (content, ns) {
                    var path = '';

                    _this5.options.src.forEach(function (dir) {
                        var component = '';

                        if (dir.prefix) {
                            // && dir.prefix === ns.substr(0, dir.prefix.length)) {
                            component = ns.substr(dir.prefix.length);
                            component = component[0].toUpperCase() + component.substr(1);
                        } else {
                            component = ns[0].toUpperCase() + ns.substr(1);
                        }

                        if (_fs2.default.existsSync(dir.path + '/' + component)) {
                            path = dir.path + '/' + component;
                        }
                    });

                    if (path) {
                        if (!_fs2.default.existsSync(path + '/i18n')) {
                            _fs2.default.mkdirSync(path + '/i18n');
                        }

                        _fs2.default.writeFileSync(path + '/i18n/' + lang + '.json', JSON.stringify(_defineProperty({}, ns, content), null, '  '));
                        res.push(path + '/i18n/' + lang + '.json');
                    }
                });
            });
            return res;
        }
    }, {
        key: 'writeToDist',
        value: function writeToDist(osJson) {
            var _this6 = this;

            var res = [];
            (0, _lodash.mapKeys)(osJson, function (NSs, lang) {
                var path = _this6.options.output;

                if (!_fs2.default.existsSync(path)) _fs2.default.mkdirSync(path);

                _fs2.default.writeFileSync(path + '/' + lang + '.js', 'export default ' + JSON.stringify(NSs, null, '  '));
                res.push(path + '/' + lang + '.js');
            });
            return res;
        }
    }]);

    return FileSystem;
}();

exports.default = FileSystem;

/***/ })
/******/ ]);