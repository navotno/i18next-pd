import {mapKeys, assign} from 'lodash';
import path from 'path';
import fs from 'fs';
import cla from 'command-line-args';

import Log from './src/Logger';
import Scanner from './src/Scanner';
import OneSky from './src/OneSky';
import Formatter from './src/Formatter';
import FileSystem from './src/FileSystem';

const optionDefinitions = [
    { name: 'scan', alias: 's', type: Boolean },
    { name: 'build', alias: 'b', type: Boolean },
    { name: 'component', alias: 'c', type: String },
    { name: 'osp', type: String },
    { name: 'osg', type: String },
    { name: 'config', type: String },
    { name: 'debug', alias: 'd', type: Boolean },
    { name: 'write', alias: 'w', type: Boolean }
];

const args = cla(optionDefinitions);
let config = {};

try {
    const configPath = path.join(process.cwd(), '/', args.config);
    config = JSON.parse(fs.readFileSync(configPath));
    Log.bold('✓ Config loaded', '#00a73d');
    Log.bold(`Path: ${configPath}`, '#00a73d');
} catch (e) {
    Log.bold('✖ Config file not found\n', '#d55413');
    Log.reset(`Path: ${path.join(process.cwd(), '/', args.config || '')}`, '#d55413');
    Log.reset(`${e}`, '#d55413');
    process.exit();
}

const fileSystem = new FileSystem(config.FileSystem);
const onesky = new OneSky(config.OneSky);
const formatter = new Formatter();

const i18nJson = { en: {} };

// Log.clean();

const setConfig = (options) => {
    config = options;
};
const getUptime = () => ((process.uptime() * 1000).toString().substr(0, 3));
const oneSkyGet = (filename) => {
    const start = process.uptime();

    Log.osg(filename);

    onesky.get(filename).then((res) => {
        const time = ((process.uptime() - start) * 1000).toString().substr(0, 3);
        const data = formatter.oneskyOutToI18n(res);

        Log.bold('     ✓ Successful', '#00a73d');
        Log.bold(`       (${time}ms)`, '#d55413');

        if (args.debug) {
            Log.boldDim('\nResult:');
            Log.reset(JSON.stringify(data, null, 4), '#aa9d00');
        }

        if (args.write) {
            const srcFiles = fileSystem.writeOSJson(data);

            Log.boldDim('\nRecorded source files:');
            srcFiles.forEach(file => Log.reset(`         ${file}`, '#aa9d00'));
        }

        Log.upt(getUptime());
    }).catch(err => Log.error(err, getUptime()));
};
const scan = () => {
    Log.bold('Scanning project');
    if (!args.debug) Log.boldDim('Components:');

    const { components } = fileSystem.run();

    mapKeys(components, (component, ns) => {
        if (args.component && args.component !== component.name) return;

        const scanner = new Scanner(
            component.content,
            {
                ns: [ns],
                defaultNs: ns,
                func: { list: ['t'], extensions: ['.js', '.jsx'] },
                resourcePath: component.path
            }
        );
        const scanResult = scanner.run();

        assign(i18nJson.en, scanResult.en);

        let files = [];
        if (args.write) {
            files = fileSystem.writeToFile(component.path, scanResult);
        }

        if (args.debug) {
            Log.bold('\nComponent name:', '#00a73d');
            Log.reset(`     ${component.name}`, '#aa9d00');
            Log.bold('\nNamespace:', '#00a73d');
            Log.reset(`     ${ns}`, '#aa9d00');

            Log.bold('\nComponent path:', '#00a73d');
            Log.reset(`     ${component.path}`, '#aa9d00');

            if (files.length) {
                Log.bold('\nRecorded files:', '#00a73d');
                Log.reset(files.join(', '), '#aa9d00');
            }

            Log.bold('\nScanning result:', '#00a73d');
            Log.reset(JSON.stringify(scanResult, null, 4), '#aa9d00');
        } else {
            Log.bold(`     ✓ ${component.name}`, '#00a73d');
        }
    });
};
const oneSkyPut = (filename) => {
    const start = process.uptime();
    scan();

    Log.osp(filename);

    onesky.put(filename, i18nJson).then((res) => {
        const time = ((process.uptime() - start) * 1000).toString().substr(0, 3);
        const result = JSON.parse(res.toString());

        if (result.meta.status !== 201) {
            Log.bold(`\n     ✖ Unsuccessful (${time}ms)`, '#d55413');
        } else {
            Log.bold('     ✓ Successful', '#00a73d');
            Log.bold(`       (${time}ms)`, '#d55413');

            if (args.debug) {
                const data = result.data;
                Log.boldKV('     ID: ', data.import.id);
                Log.boldKV('     Name: ', data.name);
                Log.boldKV('     Format: ', data.format);
                Log.boldKV('     Language: ', data.language.local_name);
            }

            Log.upt(getUptime());
        }
    }).catch(err => Log.error(err, getUptime()));
};
const build = () => {
    const result = fileSystem.build();

    Log.bold('\nBuilding i18next bundles');
    Log.bold('\nRecorded files:', '#00a73d');
    Log.reset(`    ${result.files.join('\n    ')}`, '#aa9d00');

    if (args.debug) {
        Log.bold('\nResult i18nextJSON:', '#00a73d');
        Log.reset(JSON.stringify(result.i18nextJson, null, '  '), '#aa9d00');
    }
};

if (args.build) build();
if (args.scan) scan();
if (args.osg) oneSkyGet(args.osg);
if (args.osp) oneSkyPut(args.osp);

if (!args.osp && !args.osg) Log.upt(getUptime());

module.exports = {
    scan,
    oneSkyGet,
    oneSkyPut,
    setConfig
};
