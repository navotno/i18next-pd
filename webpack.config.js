var path = require('path');
var nodeExternals = require('webpack-node-externals');

module.exports = {
    entry: ["babel-polyfill","./index.js"],
    target: 'node',
    externals: [nodeExternals()],
    output: {
        path: __dirname + '/dist',
        filename: "index.js"
    },
    resolve: {
        modules: [
            path.resolve('./node_modules'),
            path.resolve('./src')
        ],
        extensions: ['.js', '.jsx']
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ]
    }
};
